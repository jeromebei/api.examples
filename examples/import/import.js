const request = require('request');
const fs = require('fs');
const path = require('path');
const token=require("../../token/token.json");

request.post({url: token.server + "/rest/admin/users/actions/import",
  headers: {"X-Accellion-Version": token.apiVersion, "Accept": "application/json"},
  formData: {
    updateIfExists: "true",
    partialSuccess: "true",
    sendNotification: "false",
    content: fs.createReadStream(path.join(__dirname,"import.csv"))
  }},(error, response, body) => {
    if (error) return console.log(error);
    return console.log(body);
  }).auth(null, null, true, token.access_token);
