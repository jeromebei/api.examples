# Import Examples

## import.js

### Description

Programmatically import user account data into the Accellion user directory from a local CSV file. The CSV file to import from has to be named "import.csv" and reside in the script's folder.

import.csv format:

"email","name","user_type_id","mobile_number"

### Usage

    node import.js
    