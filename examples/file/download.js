const request = require('request');
const fs = require('fs');
const args = require('command-line-args');
const token = require('../../token/token.json');

const optionDefinitions = [
  { name: 'id', alias: 'i', type: Number },
  { name: 'name', alias: 'n', type: String },
]
const options = args(optionDefinitions);

request.get({url: `${token.server}/rest/files/${options.id}/content`, headers: {"X-Accellion-Version": token.apiVersion, "Accept": "application/json"}
},(error, response, body) => {
    if (error) return console.log(error);
    return console.log("done");
}).auth(null, null, true, token.access_token).pipe(fs.createWriteStream(options.name));
