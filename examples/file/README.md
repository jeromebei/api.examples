# File Examples

## download.js

### Description

Basic example to download a file by id from the Accellion store.

### Usage

    node download.js --id="<FILE ID>" --name="<LOCAL FILE PATH / NAME>"
    