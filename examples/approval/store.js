const sqlite3 = require('sqlite3').verbose();
const { doesNotThrow } = require('assert');
const fs = require('fs');
const path = require('path');

const STORE = {
    // ----------------------------------------------------------------------------------------
    file: path.join(__dirname,'approvals.db'),
    sql: null,
    // ----------------------------------------------------------------------------------------
    init: ()=>{
        if (!fs.existsSync(STORE.file)) {
            console.log(`...creating sqlite database: ${STORE.file}`);
            STORE.sql = new sqlite3.Database(STORE.file);
            var result;
            STORE.sql.exec("CREATE TABLE approvals (approver VARCHAR(256), emailId INTEGER, fileId INTEGER, fingerprint VARCHAR(64), date INTEGER, result BOOLEAN);");
            console.log(`...database created`);
            STORE.sql.exec("CREATE INDEX approvals_idx1 ON approvals(emailId);");
            STORE.sql.exec("CREATE INDEX approvals_idx2 ON approvals(fileId);");
            STORE.sql.exec("CREATE INDEX approvals_idx3 ON approvals(fingerprint);");
            console.log(`...indexes created`);
            return STORE.sql;
        }
        if (!STORE.sql) STORE.sql = new sqlite3.Database(STORE.file);
        return STORE.sql;
    },
    // ----------------------------------------------------------------------------------------
    run: async function(cmd,opts){
        //console.log(`...executing: ${cmd} with ${JSON.stringify(opts)}`);
        return await STORE.init().run(cmd,opts);
    },
    // ----------------------------------------------------------------------------------------
    get: function(cmd,opts,done) {
        //console.log(`...executing: ${cmd} with ${JSON.stringify(opts)}`);
        STORE.init();
        STORE.sql.get(cmd,opts,(err,row)=>{
            if (err) throw(err);
            return done(row);
        });
    }
    // ----------------------------------------------------------------------------------------
}
module.exports=STORE;
