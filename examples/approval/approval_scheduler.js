const fs = require('fs');
const request = require('../../lib/request.js');
const store = require('./store.js');

const INTERVAL=10000;
var timerPtr;
// ----------------------------------------------------------------------------------------
(function check(){
    console.log("...checking for new approvals");
    request.get("/mail?read=false&with=(body)&bucket=inbox",true,(response,body)=>{
        var approvalEmails = body.data.filter((el)=>{return el.subject.indexOf("APPROVAL REQUEST")>-1});
        if (approvalEmails.length==0) {
            console.log(`no new approval emails in inbox, next check in ${INTERVAL}`);
            timerPtr = setTimeout(check,INTERVAL);
            return;
        }
        console.log(`...found ${approvalEmails.length} new approval emails in inbox`);
        handleApprovals(approvalEmails,()=>{
            console.log(`all approvals processed, next check in ${INTERVAL}`);
            timerPtr = setTimeout(check,INTERVAL);
            return;            
        });
    },errorHandler);
})();
// ----------------------------------------------------------------------------------------
function handleApprovals(emails,done,idx){
    if (!idx) idx=0;
    if (idx>=emails.length) return done();
    console.log(`...processing ${idx+1} of ${emails.length} approvals`);
    var email = emails[idx];
    var tokens = email.subject.match(/APPROVAL REQUEST \[(.\d*(?=\]))\] \[(.*(?=\]))\]/);
    var fileId = parseInt(tokens[1]);
    var fingerprint = tokens[2];
    var result = email.body.match(/^<[^>]*>([^<]*)/)[1].toLowerCase().trim();
    var approved;
    switch (result) {
        case "approve": approved = true; break;
        case "reject": approved = false; break;
        default: {
            console.log(`unable to parse approval for ${result}, please handle`);
            idx++;
            return handleApprovals(emails,done,idx);
        }
    }
    console.log(`...file id ${fileId} with fingerprint ${fingerprint} has been ${approved ? "approved":"rejected"}`);
    store.get("SELECT * FROM approvals WHERE fileId=? AND fingerprint=? AND result IS NULL",[fileId,fingerprint],(row)=>{
        if (!row) {
            console.log(`...no pending approval found for file id ${fileId} and fingerprint ${fingerprint}`);
            idx++;
            return handleApprovals(emails,done,idx);
        }
        console.log(`...pending approval email found for file id ${fileId} and fingerprint ${fingerprint}: ${row.emailId}`);
        (async () => {
            var result = await store.run("UPDATE approvals SET result=? WHERE fileId=? AND fingerprint=?",[
                approved,
                fileId,
                fingerprint
            ]);
        })();
        console.log(`...database updated for file id ${fileId} and fingerprint ${fingerprint}: ${approved ? "approved":"rejected"}`);
        request.patch("/mail/actions/read?emailId:in="+email.id,false,(response,body)=>{
            console.log(`...email marked as read: ${email.id}`);
            console.log(`-->> FURTHER PROCESS APPROVAL`);
            idx++;
            return handleApprovals(emails,done,idx);
        },errorHandler);
    });
}
// ----------------------------------------------------------------------------------------
function errorHandler(err) {
    console.log(`ERROR: ${err}`);
    process.exit(1);
}
// ----------------------------------------------------------------------------------------
