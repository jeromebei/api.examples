# Approval Example

### Description

Demonstrates how Accellion secure email can be used for a document approval workflow.

The approval_request.js script triggers a document approval flow, which sends a secure email with an attachment to a user for approval. The user then reviews the document, and replies to the secure email while putting either "approve" or "reject" in the answer's email body.

In parallel, the approval_scheduler.js script watches the sender's inbox for approval answers and allows to trigger further processing once a document approval or rejection comes in. The scheduler executes every 10 seconds by default.

These scripts use an sqlite database to persist the approval request state. Also, the secure email created by the approval_request.js script puts the attached documents' id and fingerprint into the messages' subject, which then is parsed when capturing the receivers' answer.

### Usage

    node approval_request.js --file="<LOCAL FILE TO APPROVE>" --user="<APPROVER'S EMAIL>"
    node approval_scheduler.js
    