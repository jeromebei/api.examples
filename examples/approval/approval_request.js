const fs = require('fs');
const args = require('command-line-args');
const request = require('../../lib/request.js');
const store = require('./store.js');

const optionDefinitions = [
    { name: 'file', alias: 'f', type: String },
    { name: 'user', alias: 'u', type: String }
]
const options = args(optionDefinitions);
// ----------------------------------------------------------------------------------------
var userObj=null;
var fileObj=null;
var emailObj=null;
console.log(`...getting user account information`);
request.get("/users/me",true,(response,body)=>{
    userObj=body;
    console.log(`...uploading file to folder id ${userObj.mydirId}`);
    request.post("/folders/" + userObj.mydirId + "/actions/file?returnEntity=true", {file: fs.createReadStream(options.file)},true,(response,body)=>{
        fileObj=body;
        console.log(`...file uploaded with id ${fileObj.id}`);
        console.log(`...sending email to approver ${options.user}`);
        var formData = {
            to:[options.user],
            files: [fileObj.id],
            type: "original",
            subject: `APPROVAL REQUEST [${fileObj.id}] [${fileObj.fingerprint}] (${fileObj.name})`,
            body: `Please review the enclosed document ${fileObj.name}.\n\nTo approve, please reply to this email with the text "APPROVE" in the message body, to reject, please type "REJECT" in your reply.`,
        };
        request.post("/mail/actions/sendFile?returnEntity=true",formData,true,(response,body)=>{
            emailObj = body;
            (async () => {
                var result = await store.run("INSERT INTO approvals (approver,emailId,fileId,fingerprint,date) VALUES(?,?,?,?,?)",[
                    options.user,
                    emailObj.id,
                    fileObj.id,
                    fileObj.fingerprint,
                    Date.now()
                ]);
            })();
            console.log(`done`);
        },errorHandler);        
    },errorHandler);
},errorHandler);

// ----------------------------------------------------------------------------------------
function errorHandler(err) {
    console.log(`ERROR: ${err}`);
    process.exit(1);
}
// ----------------------------------------------------------------------------------------
