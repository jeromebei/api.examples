const request = require('request');
const fs = require('fs');
const token=require("../../token/token.json");

request.get({url: token.server + "/rest/activities",
  headers: {"X-Accellion-Version": token.apiVersion, "Accept": "application/json"}
  },(error, response, body) => {
    if (error) return console.log(error);
    return console.log(body);
  }).auth(null, null, true, token.access_token);
