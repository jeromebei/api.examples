# Accellion API Usage Examples
A collection of [Node.JS](https://nodejs.org/) based [Accellion](https://www.accellion.com) API usage examples. 

## Requirements
In order to run the examples, you will need to install [Node.JS](https://nodejs.org/) on your system and have backend access to an [Accellion](https://www.accellion.com) Content Firewall that is licensed with the REST Api option. 

## Installation
Download / clone the repository, then run the following command inside the download folder:

    npm install

## Project structure
![Structure](https://bitbucket.org/jeromebei/api.examples/raw/HEAD/assets/structure.png)

## Configuration
Before running the example code, you will need to configure a custom application in the backend of the [Accellion](https://www.accellion.com) Content Firewall. Please refer to the Accellion Administrator's Guide, section "Custom Applications" to learn how to create a custom application. 

In order to create a token, the example code requires you to configure the custom application with the "Authorization Code" flow. Please remember the Client ID and Secret once the custom application has been created in Accellion.

To generate a token to be used by the examples, open a command prompt and CD into the repositories root folder. Then run the following command:

    node ./lib/get_token.js --clientid="<YOUR CLIENT ID>" --secret="<YOUR SECRET>" --host="<YOUR ACCELLION HOST>" --port=8080

N.B. If port 8080 is already in use on your system, change the --port argument to an unused port.

This will start a local server instance. With your favorite web browser, navigate to http://localhost:8080/login

Login with your Accellion credentials and grant access to your custom application. Once completed, the token retrieved from the Accellion backend will be stored inside the token/token.json file.

All of the examples accessing the Accellion Rest Api will use the token.json file to authenticate; hence make sure the file exists before running any of the example code.

## Example Documentation

- [User](./examples/user/)
- [File](./examples/file/)
- [Import](./examples/import/)
- [Email](./examples/email/)
- [Approval Workflow](./examples/approval/)

## Disclaimer 

THIS SAMPLE CODE IS PROVIDED “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) SUSTAINED BY YOU OR A THIRD PARTY, HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SAMPLE CODE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.