const express = require('express');
const querystring = require('querystring');
const https = require('https');
const fs = require('fs');
const path = require('path');
const args = require('command-line-args');

var app = express();

const optionDefinitions = [
  { name: 'clientid', alias: 'c', type: String },
  { name: 'secret', alias: 's', type: String },
  { name: 'host', alias: 'h', type: String },
  { name: 'port', alias: 'p', type: Number },
  { name: 'file', alias: 'f', type: String }
]
const options = args(optionDefinitions);
const API_VERSION="17";
const CLIENT_ID=options.clientid;
const CLIENT_SECRET=options.secret;
const FILENAME = options.file ? options.file : "token.json";

const PROTOCOL="https://";
const SERVER=options.host;
const REDIRECT_URI = "http://localhost:"+options.port+"/oauth";

//Web Browser calling /login
app.get('/login', function (req, res) {
  var authUri = PROTOCOL+SERVER + "/oauth/authorize?client_id=" + CLIENT_ID + "&response_type=code&scope=&redirect_uri=" + encodeURIComponent(REDIRECT_URI);
  res.redirect(authUri);
});

//Accellion server calling /oauth
app.get('/oauth', function (req, res) {
  var code = req.query.code;
  var form = {
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      grant_type: "authorization_code",
      redirect_uri: encodeURIComponent(REDIRECT_URI),
      code: code
  };
  var formData=querystring.stringify(form);
  var options = {
    hostname: SERVER,
    port: 443,
    path: '/oauth/token',
    method: 'POST',
    headers: {'Content-Type': 'application/x-www-form-urlencoded','Content-Length': formData.length}
  };
  const req2 = https.request(options, res2 => {
    res2.on('data', d => {
      var tokenData = JSON.parse(d.toString('utf8'));
      tokenData.apiVersion=API_VERSION;
      tokenData.clientId=CLIENT_ID;
      tokenData.clientSecret=CLIENT_SECRET;
      tokenData.server=PROTOCOL+SERVER;
      if (!fs.existsSync(path.join(__dirname,"..","token"))) fs.mkdirSync(path.join(__dirname,"..","token"),{recursive:true});
      fs.writeFileSync(path.join(__dirname,"..","token",FILENAME),JSON.stringify(tokenData));
      console.log(`token stored in: ${path.join(__dirname,"..","token",FILENAME)}`);
      res.send("Token retrieved and stored.");
      process.exit(0);
    });
  });
  req2.on('error', (e) => {
    console.error(e);
  });

  req2.write(formData);
  req2.end();
});


app.listen(options.port, function () {
  console.log(`listening on port ${options.port}`);
  console.log(`point your browser to http://localhost:${options.port}/login to connect`);
});
