const request = require('request');
const fs = require('fs');
const token=require("../token/token.json");

// ----------------------------------------------------------------------------------------
function get(endpoint,parse,done,failure) {
    request.get({url: token.server + "/rest"+endpoint,
    headers: {"X-Accellion-Version": token.apiVersion, "Accept": "application/json"},
  },(error, response, body) => {
      if (error) return failure(error);
      if (!parse) return done(response,body);
      else {
          try {
              body = JSON.parse(body);
              if (body.errors && body.errors.length>0)  return failure(JSON.stringify(body.errors[0]));
              return done(response,body);
          } catch (error) {
              return failure(error);
          }
      }
    }).auth(null, null, true, token.access_token);
}
// ----------------------------------------------------------------------------------------
function patch(endpoint,parse,done,failure) {
    request.patch({url: token.server + "/rest"+endpoint,
    headers: {"X-Accellion-Version": token.apiVersion, "Accept": "application/json"},
  },(error, response, body) => {
      if (error) return failure(error);
      if (!parse) return done(response,body);
      else {
          try {
              body = JSON.parse(body);
              if (body.errors && body.errors.length>0)  return failure(JSON.stringify(body.errors[0]));
              return done(response,body);
          } catch (error) {
              return failure(error);
          }
      }
    }).auth(null, null, true, token.access_token);
}
// ----------------------------------------------------------------------------------------
function post(endpoint,formData,parse,done,failure){
    request.post({url: token.server + "/rest"+endpoint,
    headers: {"X-Accellion-Version": token.apiVersion, "Accept": "application/json"},
    formData: formData},(error, response, body) => {
      if (error) return failure(error);
      if (!parse) return done(response,body);
      else {
        try {
            body = JSON.parse(body);
            if (body.errors && body.errors.length>0)  return failure(JSON.stringify(body.errors[0]));
            return done(response,body);
        } catch (error) {
            return failure(error);
        }
    }      
    }).auth(null, null, true, token.access_token);    
}
// ----------------------------------------------------------------------------------------
exports.get=get;
exports.post=post;
exports.patch=patch;
// ----------------------------------------------------------------------------------------
